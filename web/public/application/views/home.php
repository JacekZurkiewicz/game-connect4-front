<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Connect4</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url('assets/css//bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css//AdminLTE.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/_all-skins.min.css');?>">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="#" class="logo">
            <span class="logo-lg">Connect<b>4</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <button type="button" class="btn btn-danger pull-right"
                    style="margin-top:7px;margin-right:20px"
                    data-toggle="modal" data-target="#modal-danger">
                Wychodzę
            </button>
        </nav>
    </header>
    <div style="background-color: #ecf0f5">
        <section class="content-header">
            <h1>
                Rozgrywka
                <small> Nr. </small>
                <small id="myBattle"></small>
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-body no-padding" id="battleBoard">
                            <p class="lead">
                                <a href="#" onclick="javascript:WebSocketOpen();"
                                   class="btn bg-maroon margin-top"
                                   style="margin-top: 20px;
                                       margin-left: 50%;
                                       margin-right: 50%">
                                    <strong>Join battle</strong>
                                </a>
                            </p>
                        </div>
                        <div class="box-footer no-padding"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-solid limit-p-width">
                        <div class="box-body affiliate">
                            <div class="heading">
                                Twój nick: <strong id="myNick">Start Game</strong>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="myColor" class="btn btn-block margin-bottom">
                        <strong style="color: #ffffff"></strong></a>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gracze</h3>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked" id="players">
                            </ul>
                        </div>
                    </div>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Komuniaty</h3>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#" class="text-red"> ruch player 2</a></li>
                                <li><a href="#" class="text-yellow"> ruch player 1</a></li>
                                <li><a href="#" class="text-light-blue"> gra została rozpoczęta</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal modal-danger fade" id="modal-danger">

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script>

    var ws;

    function WebSocketOpen() {


        ws = new WebSocket("ws://localhost:8085");

        ws.onopen = function (data) {
            console.log('start - open');

            ws.send(JSON.stringify(
                {
                    'topic' : '/battles/join'
                }
                )
            );
        }

        ws.onclose = function (data) {
            console.log(data);
        }

        ws.onmessage = function (jsonFile) {

            var message =  JSON.parse(jsonFile.data);
            var topic = message.topic;
            var payload = message.payload;

            switch (topic) {
                case 'init':
                    drawBattleData(payload);
                    break;
                case 'checkdeadline':
                    updateBattleData(payload);
                    break;
                case 'currentBattleData':
                    console.log(payload);
                    updateBattleData(payload);
                    break;
                default:
                    console.log('Brak poprawnego tematu');
            }
        }
    }

    function drawBattleData(payload) {
        var colorPlayer = getColor(payload.color);
        var nick = document.getElementById('myNick');
        var battle = document.getElementById('myBattle');

        createPlayerMenu(payload);
        // todo - tu dodaje nick i porownje go z payload

        drawBoard(payload, currentPlayer(payload));

        color = document.getElementById('myColor');
        color.innerText = 'Twój kolor: ';

        color.setAttribute("style", "background-color:"+colorPlayer+"; color: #ffffff; font-weight: bold;");
        color.append(colorPlayer);

        nick.innerText = '';
        battle.innerText = '';
        nick.append(payload.nick);
        battle.append(payload.battleId);

    }

    function drawBoard(payload, currentPlayer) {
        var board = payload.matrix;
        var div = document.getElementById('battleBoard');
        var table = document.createElement("table");
        table.setAttribute("class", "table table-bordered");

        for(rowNumber in board){

            if (rowNumber == 1){
                var thead = document.createElement("thead");
                var elementThead = table.appendChild(thead);
                var headRow = document.createElement("tr");
                var headRowTh = elementThead.appendChild(headRow);
                var cellHeadTh = document.createElement("th");

                cellHeadTh.setAttribute("scope", "col");
                headRowTh.appendChild(cellHeadTh);

                for(columnNumber in board[rowNumber]){

                    var cellHeadTh = document.createElement("th");
                    cellHeadTh.setAttribute("scope", "col");
                    headRowTh.appendChild(cellHeadTh);
                    txt = document.createTextNode(columnNumber);

                    var buttonCellHeadTh = document.createElement("a");
                        buttonCellHeadTh.setAttribute('href', "#");

                        if(currentPlayer){
                            buttonCellHeadTh.setAttribute('class', "btn bg-maroon margin-top");
                            buttonCellHeadTh.setAttribute('onclick', "button"+columnNumber);
                        } else {
                            buttonCellHeadTh.setAttribute('class', "btn bg-gray margin-top");
                        }

                        buttonCellHeadTh.setAttribute('style', "weight:100%");
                        buttonCellHeadTh.appendChild(txt);
                        cellHeadTh.appendChild(buttonCellHeadTh);
                }
                var tbody = document.createElement("tbody");
                var elementTbody = table.appendChild(tbody);
            }

            var bodyRow = document.createElement("tr");
            var bodyRowTd = elementTbody.appendChild(bodyRow);
            var cellBodyTh = document.createElement("th");
            var txt = document.createTextNode(rowNumber);

            cellBodyTh.setAttribute("scope", "row");
            bodyRow.appendChild(cellBodyTh);
            cellBodyTh.appendChild(txt);

            for(columnNumber in board[rowNumber]){

                var cell = document.createElement("td");
                bodyRowTd.appendChild(cell);

            }
        }

        div.innerHTML = '';
        div.append(table);
    }
/*
todo

        KOMUNIKATY - opcja
                    - po przekroczeniu czasu
                    - po wyjsciu jednego z userow

 */

    function updateBattleData(payload) {

        createPlayerMenu(payload);
        changeActivePlayer(payload);
        drawBoard(payload, currentPlayer(payload));

    }

    function getColor(colorId) {

        colors = [
            'red', 'green', 'blue', 'pink', 'yellow'
        ];

        return colors[colorId];
    }

    function startTimer(duration, display) {

        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;
            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    };

    /**
     * Zaznacza który player ma obecnie wykonać ruch
     */
    function changeActivePlayer(payload) {

        var endTime = payload.deadline;
        var nowTime = Math.round(Date.now() / 1000);

        li = document.getElementById(payload.player.nick);
        span = li.getElementsByClassName("label")[0];
        li.setAttribute("class", "active");

        deadLine = endTime - nowTime;

        startTimer(deadLine, span);

    }

    /**
     *
     * Prawa strona gry z informacjami o graczach
     *
     * @param payload
     */
    function createPlayerMenu(payload) {
        var players = payload.players;
        var ul = document.getElementById('players');
        ul.innerText = '';

        for (var i=0; i<=players.length;i++){

            var li = document.createElement("li");
            var aHref = document.createElement("a");
            var icon = document.createElement("i");
            var span = document.createElement("span");
            var liId = document.getElementById("li");

            ul.appendChild(li);

            if (players[i] != liId){
                li.setAttribute("id", players[i]);

                var txt = document.createTextNode(players[i]);

                icon.setAttribute("class", "fa fa-inbox");
                span.setAttribute("class", "label label-primary pull-right");

                li.appendChild(aHref);
                aHref.appendChild(icon);
                aHref.appendChild(txt);
                aHref.appendChild(span);
            }
        }
    }

    /**
     *
     * Sprawdzenie czy jest kolej gracza obecnie wyświetlającego grę.
     *
     * @param payload
     * @returns {boolean}
     */
    function currentPlayer(payload) {
        var nick = payload.nick;

        if ((typeof(payload.player) != "undefined" && payload.player == nick)){
            return true;
        } else {
            return false;
        }
    }

</script>
<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/fastclick.js')?>"></script>
<script src="<?php echo base_url('assets/js/adminlte.min.js')?>"></script>
</body>
</html>
