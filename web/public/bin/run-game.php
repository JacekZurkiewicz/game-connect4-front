<?php
use Ratchet\Server\IoServer;
use MyApp\ConnectGame;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
    new ConnectGame(),
    8085
);

$server->run();
