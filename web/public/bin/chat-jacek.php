<?php
use Ratchet\Server\IoServer;
use MyApp\MyChat;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
    new MyChat(),
    8085
);

$server->run();
